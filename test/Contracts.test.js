const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const factory = require('../src/eth/build/Factory.json');
const campaign = require('../src/eth/build/Campaign.json');

const provider = ganache.provider();
const web3 = new Web3(provider);

const GAS = '1000000';
const CONTRIBUTION_MIN = '0.05';
const CONTRIBUTION_LOW = '0.02';
const CONTRIBUTION_SIZE = '0.15';
const REQUEST_VALUE = '0.05';
const REQUEST_REASON = 'Buy materials';

let accounts;
let fact;
let camp;
let campAddress;

beforeEach(async () => {
  accounts = await web3.eth.getAccounts();

  fact = await new web3.eth.Contract(JSON.parse(factory.interface))
    .deploy({
      data: factory.bytecode
    })
    .send({ from: accounts[0], gas: '1000000' });

  fact.setProvider(provider);

  await fact.methods
    .createCampaign(web3.utils.toWei(CONTRIBUTION_MIN, 'ether'))
    .send({
      from: accounts[0],
      gas: GAS
    });

  [campAddress] = await fact.methods.getCampaigns().call();

  camp = await new web3.eth.Contract(
    JSON.parse(campaign.interface),
    campAddress
  );
  camp.setProvider(provider);
});

describe('Campaigns', () => {
  it('deploys factory and campaign', () => {
    assert.ok(fact.options.address);
    assert.ok(camp.options.address);
  });

  it('marks initiator campaign manager', async () => {
    const manager = await camp.methods.manager().call();

    assert.equal(manager, accounts[0]);
  });

  it('allows patrons to join', async () => {
    await camp.methods.contribute().send({
      from: accounts[1],
      value: web3.utils.toWei(CONTRIBUTION_SIZE, 'ether')
    });

    const isPatron = await camp.methods.patrons(accounts[1]).call();

    assert(isPatron);
  });

  it('enforces campaign minimum', async () => {
    try {
      await camp.methods.contribute().send({
        from: accounts[1],
        value: web3.utils.toWei(CONTRIBUTION_LOW, 'ether')
      });

      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('allows manager to make request', async () => {
    await camp.methods
      .createRequest(
        REQUEST_REASON,
        web3.utils.toWei(REQUEST_VALUE, 'ether'),
        accounts[2]
      )
      .send({
        from: accounts[0],
        gas: GAS
      });

    const req = await camp.methods.requests(0).call();

    assert.equal(REQUEST_REASON, req.desc);
  });

  it('denies non manager making request', async () => {
    try {
      await camp.methods
        .createRequest(
          REQUEST_REASON,
          web3.utils.toWei(REQUEST_VALUE, 'ether'),
          accounts[2]
        )
        .send({
          from: accounts[1],
          gas: GAS
        });

      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('completes requests', async () => {
    await camp.methods.contribute().send({
      from: accounts[1],
      value: web3.utils.toWei(CONTRIBUTION_SIZE, 'ether')
    });

    await camp.methods
      .createRequest(
        REQUEST_REASON,
        web3.utils.toWei(REQUEST_VALUE, 'ether'),
        accounts[2]
      )
      .send({
        from: accounts[0],
        gas: GAS
      });

    await camp.methods.approveRequest(0).send({
      from: accounts[1],
      gas: GAS
    });

    await camp.methods.completeRequest(0).send({
      from: accounts[0],
      gas: GAS
    });

    var bal = await web3.eth.getBalance(accounts[2]);
    bal = web3.utils.fromWei(bal, 'ether');
    bal = parseFloat(bal);

    assert(bal > 100);
  });

  // non patron approve request
  // patron double contributes
  // non manager complete request
  // manager complete request without any votes
  // request payment greater than balance
});

pragma solidity ^0.4.17;

contract Factory {

    address[] public deployedCampaigns;

    function createCampaign(uint min) public {
        address newCampaign = new Campaign(min, msg.sender);

        deployedCampaigns.push(newCampaign);
    }

    function getCampaigns() public view returns(address[]) {
        return deployedCampaigns;
    }
}

contract Campaign {

    struct Request {
        string desc;
        uint value;
        address recipient;
        bool complete;
        uint yesCount;
        mapping(address=>bool) voters;
    }

    uint public minContribution;
    address public manager;
    mapping(address=>bool) public board;
    mapping(address=>bool) public patrons;
    uint patronCount;
    Request[] public requests;
    

    function Campaign(uint min, address creator) public {
        manager = creator;
        minContribution = min;
    }

    modifier managerRequired() {
        require(msg.sender == manager);
        _;
    }

    modifier boardRequired() {
        _;
    }

    modifier patronRequired() {
        require(patrons[msg.sender]);
        _;
    }

    function getRequestCount() public view returns(uint) {
        return requests.length;
    }

    function getSummary() public view returns(uint, uint, uint, uint, address) {
        return(minContribution, this.balance, requests.length, patronCount, manager);
    }

    function contribute() public payable {
        require(msg.value >= minContribution);

        if (!patrons[msg.sender]) {
            patrons[msg.sender] = true;
            patronCount++;
        }       
    }

    function createRequest(string desc, uint value, address rec) public managerRequired {

        Request memory newReq = Request({
            desc: desc,
            value: value,
            recipient: rec,
            complete: false,
            yesCount: 0
        });

        requests.push(newReq);
    }

    function approveRequest(uint index) public patronRequired {
        Request storage req = requests[index];

        require(!req.voters[msg.sender]);

        req.voters[msg.sender] = true;
        req.yesCount++;
    }

    function completeRequest(uint index) public managerRequired {
        Request storage req = requests[index];

        require(!req.complete);
        require(req.yesCount > (patronCount/2));

        req.recipient.transfer(req.value);
        req.complete = true;
    }
}
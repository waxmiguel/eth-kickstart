import web3 from './web3';
import factory from './build/Factory.json';

const instance = new web3.eth.Contract(
  JSON.parse(factory.interface),
  '0x68FA00d3F06e75D1Ba074d2425b67e0AF401377C'
);

export default instance;

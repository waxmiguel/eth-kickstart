import React, { Component } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { Container } from 'semantic-ui-react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';

import routes from './routes';
import { NoMatch } from './loader';
import Header from './components/Header';
import * as actions from '../../actions';

const TITLE = 'Eth-Kickstart';

class App extends Component {
  state = {};

  componentDidMount() {}

  renderRoutes = () => {
    return routes.map(route => {
      return <Route key={route.title} {...route} />;
    });
  };

  render() {
    return (
      <Router>
        <div className="App">
          <Helmet titleTemplate={`%s - ${TITLE}`} />
          <Header />
          <Container>
            <Switch>
              {this.renderRoutes()}
              <Route component={NoMatch} />
            </Switch>
          </Container>
        </div>
      </Router>
    );
  }
}

const mapStateToProps = (state = {}) => {
  return {
    auth: state.auth
  };
};

export default connect(mapStateToProps, actions)(App);

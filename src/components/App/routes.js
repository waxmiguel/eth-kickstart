import { Index, Campaign } from './loader';

export default [
  {
    title: 'Home',
    path: '/',
    component: Index,
    exact: true
  },
  {
    title: 'Campaign',
    path: '/campaign/:address',
    component: Campaign,
    exact: true
  }
];

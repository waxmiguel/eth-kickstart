import React, { Component } from 'react';
import { Form, Input, Button, Message, Modal } from 'semantic-ui-react';
import { reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import * as actions from '../../../actions';

class NewCampaign extends Component {
  state = { min: 0 };

  async componentDidMount() {}

  onSubmit = () => {
    this.props.createCampaign(this.state.min);

    this.setState({ min: 0 });
  };

  onCancel = () => {
    this.props.cancelNewCampaign();

    this.setState({ min: 0 });
  };

  render() {
    return (
      <Modal
        dimmer="inverted"
        open={this.props.new}
        onClose={() => this.onCancel()}
      >
        <Modal.Header>Create Campaign</Modal.Header>
        <Modal.Content image>
          <Modal.Description>
            <Form error={!!this.props.error}>
              <Form.Field>
                <label>Set Minimum Contribution</label>

                <Input
                  label="ether"
                  labelPosition="right"
                  value={this.state.min}
                  onChange={e => this.setState({ min: e.target.value })}
                  type="number"
                />
              </Form.Field>
              <Message content={this.props.error} header="Error" error />
            </Form>
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button
            color="red"
            onClick={() => this.onCancel()}
            icon="cancel"
            labelPosition="right"
            content="Cancel"
          />
          <Button
            positive
            icon="checkmark"
            labelPosition="right"
            content="Create"
            loading={this.props.loading}
            onClick={() => this.onSubmit()}
          />
        </Modal.Actions>
      </Modal>
    );
  }
}

const mapStateToProps = (state = {}) => {
  const { campaignView, loadingCampaign, error } = state.view;

  return {
    contract: state.campaign.contract,
    new: campaignView,
    loading: loadingCampaign,
    error
  };
};

NewCampaign = reduxForm({
  form: 'newCampaign'
})(NewCampaign);

export default connect(mapStateToProps, actions)(NewCampaign);

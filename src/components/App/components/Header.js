import React, { Component } from 'react';
import { Menu } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../../../actions';
import NewCampaign from './NewCampaign';

class Header extends Component {
  render() {
    return (
      <Menu style={{ marginBottom: 10, borderRadius: 0 }}>
        <Menu.Item>
          <Link to="/" onClick={() => this.props.cancelNewCampaign()}>
            Kickstart
          </Link>
        </Menu.Item>

        <Menu.Menu position="right">
          <Menu.Item onClick={() => this.props.viewNewCampaign()}>+</Menu.Item>
        </Menu.Menu>
        <NewCampaign />
      </Menu>
    );
  }
}

const mapStateToProps = (state = {}) => {
  return {};
};

export default connect(mapStateToProps, actions)(Header);

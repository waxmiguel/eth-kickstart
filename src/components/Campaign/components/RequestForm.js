import React, { Component } from 'react';
import { Button, Form } from 'semantic-ui-react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import * as actions from '../../../actions';

class RequestForm extends Component {
  onSubmit = async values => {
    const { desc, value, recipient } = values;

    this.props.createRequest(desc, value, recipient, this.props.contract);
  };

  render() {
    const { handleSubmit, cancelRequest, loading, error } = this.props;

    return (
      <Form onSubmit={handleSubmit(this.onSubmit)} error={!!error}>
        <Form.Field>
          <label>Description</label>
          <Field name="desc" component="input" type="text" />
        </Form.Field>
        <Form.Field>
          <label>Ether</label>
          <Field name="value" component="input" type="number" />
        </Form.Field>
        <Form.Field>
          <label>Recipient</label>
          <Field name="recipient" component="input" type="text" />
        </Form.Field>

        <Button content="Add" primary loading={loading} />
        <Button content="Cancel" color="red" onClick={() => cancelRequest()} />
      </Form>
    );
  }
}

const mapStateToProps = (state = {}) => {
  return {
    loading: state.view.loadingRequest,
    error: state.view.error,
    contract: state.campaign.contract
  };
};

RequestForm = reduxForm({
  form: 'newRequest'
})(RequestForm);

export default connect(mapStateToProps, actions)(RequestForm);

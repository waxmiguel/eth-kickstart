import React, { Component } from 'react';
import { Table, Button } from 'semantic-ui-react';
import { connect } from 'react-redux';
import * as actions from '../../../actions';

class RequestRow extends Component {
  render() {
    const { Row, Cell } = Table;
    const {
      id,
      request,
      contract,
      loadingApprove,
      loadingComplete,
      isManager
    } = this.props;

    return (
      <Row disabled={request.complete}>
        <Cell>{id}</Cell>
        <Cell>{request.desc}</Cell>
        <Cell>{request.value}</Cell>
        <Cell>{request.recipient}</Cell>
        <Cell>{request.yesCount}</Cell>
        <Cell>
          {request.complete ? null : isManager ? (
            <Button
              basic
              color="teal"
              content="Complete"
              loading={loadingComplete}
              onClick={() => this.props.completeRequest(id, contract)}
            />
          ) : (
            <Button
              basic
              color="green"
              content="Approve"
              loading={loadingApprove}
              onClick={() => this.props.approveRequest(id, contract)}
            />
          )}
        </Cell>
      </Row>
    );
  }
}

const mapStateToProps = (state = {}) => {
  const { loadingApprove, loadingComplete } = state.view;

  const { manager, account } = state.campaign.campaign;

  return {
    loadingApprove,
    loadingComplete,
    contract: state.campaign.contract,
    isManager: account === manager
  };
};

export default connect(mapStateToProps, actions)(RequestRow);

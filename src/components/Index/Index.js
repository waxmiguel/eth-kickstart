import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Card } from 'semantic-ui-react';
import { connect } from 'react-redux';
import * as actions from '../../actions';

class Index extends Component {
  state = { campaigns: [] };

  async componentDidMount() {
    this.props.fetchCampaigns();
  }

  renderCampaigns() {
    const items = this.props.campaigns.map(addr => {
      return {
        header: addr,
        description: <Link to={`/campaign/${addr}`}>View Campaign</Link>,
        fluid: true
      };
    });

    return <Card.Group items={items} />;
  }

  render() {
    return (
      <div>
        <h3>Campaigns</h3>
        {this.renderCampaigns()}
      </div>
    );
  }
}

const mapStateToProps = (state = {}) => {
  return {
    campaigns: state.campaign.campaigns
  };
};

export default connect(mapStateToProps, actions)(Index);

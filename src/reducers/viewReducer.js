import {
  TOGGLE_APPROVE_LOADING,
  TOGGLE_CAMPAIGN_LOADING,
  TOGGLE_CAMPAIGN_VIEW,
  TOGGLE_COMPLETE_LOADING,
  TOGGLE_CONTRIBUTION_LOADING,
  TOGGLE_REQUEST_LOADING,
  TOGGLE_REQUEST_VIEW,
  DISPLAY_ERROR
} from '../actions/types';

const initialState = {
  loadingApprove: false,
  loadingCampaign: false,
  loadingComplete: false,
  loadingContribution: false,
  loadingRequest: false,
  campaignView: false,
  requestView: false,
  error: ''
};

export default (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_APPROVE_LOADING:
      return Object.assign({}, state, {
        loadingApprove: action.payload || false
      });
    case TOGGLE_CAMPAIGN_LOADING:
      console.log(TOGGLE_CAMPAIGN_LOADING, action.payload);
      return Object.assign({}, state, {
        loadingCampaign: action.payload || false
      });
    case TOGGLE_COMPLETE_LOADING:
      return Object.assign({}, state, {
        loadingComplete: action.payload || false
      });
    case TOGGLE_CONTRIBUTION_LOADING:
      return Object.assign({}, state, {
        loadingContribution: action.payload || false
      });
    case TOGGLE_REQUEST_LOADING:
      return Object.assign({}, state, {
        loadingRequest: action.payload || false
      });
    case TOGGLE_CAMPAIGN_VIEW:
      return Object.assign({}, state, {
        campaignView: action.payload || false
      });
    case TOGGLE_REQUEST_VIEW:
      return Object.assign({}, state, {
        requestView: action.payload || false
      });
    case DISPLAY_ERROR:
      if (!action.payload.includes('User denied')) {
        return Object.assign({}, state, {
          error: action.payload || ''
        });
      }

      return '';
    default:
      return state;
  }
};

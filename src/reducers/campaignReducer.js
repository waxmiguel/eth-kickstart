import { FETCH_CAMPAIGNS, FETCH_CAMPAIGN } from '../actions/types';

const initialState = {
  campaigns: [],
  campaign: {},
  contract: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CAMPAIGNS:
      return Object.assign({}, state, { campaigns: action.payload || [] });
    case FETCH_CAMPAIGN:
      return Object.assign({}, state, {
        campaign: action.payload.campaign || {},
        contract: action.payload.contract || null
      });
    default:
      return state;
  }
};

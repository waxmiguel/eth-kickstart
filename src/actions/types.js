export const FETCH_USER = 'fetch_user';

export const FETCH_CAMPAIGNS = 'fetch_campaigns';
export const FETCH_CAMPAIGN = 'fetch_campaign';

export const CREATE_CAMPAIGN = 'create_campaign';
export const CREATE_REQUEST = 'create_request';
export const ADD_CONTRIBUTION = 'add_contribution';

export const APPROVE_REQUEST = 'approve_request';
export const COMPLETE_REQUEST = 'complete_request';

export const DISPLAY_ERROR = 'display_error';

export const TOGGLE_CONTRIBUTION_LOADING = 'toggle_contribution_loading';
export const TOGGLE_REQUEST_LOADING = 'toggle_request_loading';
export const TOGGLE_CAMPAIGN_LOADING = 'toggle_campaign_loading';
export const TOGGLE_APPROVE_LOADING = 'toggle_approve_loading';
export const TOGGLE_COMPLETE_LOADING = 'toggle_complete_loading';
export const TOGGLE_REQUEST_VIEW = 'toggle_request_view';
export const TOGGLE_CAMPAIGN_VIEW = 'toggle_campaign_view';

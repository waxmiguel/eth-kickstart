import factory from '../eth/factory';
import Campaign from '../eth/campaign';
import web3 from '../eth/web3';

import {
  FETCH_CAMPAIGNS,
  FETCH_CAMPAIGN,
  TOGGLE_CONTRIBUTION_LOADING,
  DISPLAY_ERROR,
  TOGGLE_REQUEST_LOADING,
  TOGGLE_REQUEST_VIEW,
  TOGGLE_CAMPAIGN_VIEW,
  TOGGLE_CAMPAIGN_LOADING,
  TOGGLE_APPROVE_LOADING,
  TOGGLE_COMPLETE_LOADING
} from './types';

export const fetchCampaigns = () => async (dispatch, getState) => {
  const payload = await factory.methods.getCampaigns().call();

  const state = getState();

  console.log('getState: ', state);

  dispatch({ type: FETCH_CAMPAIGNS, payload });
};

export const fetchCampaign = address => async dispatch => {
  const accounts = await web3.eth.getAccounts();
  const contract = Campaign(address);
  const summary = await contract.methods.getSummary().call();

  const requests = await Promise.all(
    Array(parseInt(summary[2], 10))
      .fill()
      .map(async (_, index) => {
        const request = await contract.methods.requests(index).call();

        request.value = web3.utils.fromWei(request.value, 'ether');

        return request;
      })
  );

  const payload = {
    contract,
    campaign: {
      minContribution: web3.utils.fromWei(summary[0], 'ether'),
      balance: web3.utils.fromWei(summary[1], 'ether'),
      requestCount: summary[2],
      patronCount: summary[3],
      manager: summary[4],
      requests: requests,
      account: accounts[0]
    }
  };

  dispatch({ type: FETCH_CAMPAIGN, payload });
};

export const addContribution = (amount, contract) => async (
  dispatch,
  getState
) => {
  dispatch({ type: TOGGLE_CONTRIBUTION_LOADING, payload: true });

  try {
    const accounts = await web3.eth.getAccounts();

    await contract.methods.contribute().send({
      from: accounts[0],
      value: web3.utils.toWei(amount, 'ether')
    });

    fetchCampaign(contract.options.address);

    dispatch({ type: DISPLAY_ERROR, payload: '' });

    //fetchCampaign(contract.options.address); // refresh campaign data
  } catch (err) {
    dispatch({ type: DISPLAY_ERROR, payload: err.message });
  }

  dispatch({ type: TOGGLE_CONTRIBUTION_LOADING, payload: false });
};

export const viewRequest = () => async dispatch => {
  dispatch({ type: TOGGLE_REQUEST_VIEW, payload: true });
};

export const cancelRequest = () => async dispatch => {
  dispatch({ type: TOGGLE_REQUEST_VIEW, payload: false });
};

export const createRequest = (
  desc,
  value,
  recipient,
  contract
) => async dispatch => {
  dispatch({ type: TOGGLE_REQUEST_LOADING, payload: true });

  try {
    const accounts = await web3.eth.getAccounts();

    await contract.methods
      .createRequest(desc, web3.utils.toWei(value, 'ether'), recipient)
      .send({
        from: accounts[0]
      });

    dispatch({ type: DISPLAY_ERROR, payload: '' });
  } catch (err) {
    dispatch({ type: DISPLAY_ERROR, payload: err.message });
  }

  // refresh campaign data
  dispatch({ type: TOGGLE_REQUEST_LOADING, payload: false });
  dispatch({ type: TOGGLE_REQUEST_VIEW, payload: false });
};

export const viewNewCampaign = () => async dispatch => {
  dispatch({ type: TOGGLE_CAMPAIGN_VIEW, payload: true });
};

export const cancelNewCampaign = () => async dispatch => {
  dispatch({ type: TOGGLE_CAMPAIGN_VIEW, payload: false });
};

export const createCampaign = min => async dispatch => {
  dispatch({ type: TOGGLE_CAMPAIGN_LOADING, payload: true });

  try {
    const accounts = await web3.eth.getAccounts();

    await factory.methods.createCampaign(web3.utils.toWei(min, 'ether')).send({
      from: accounts[0]
    });

    dispatch({ type: DISPLAY_ERROR, payload: '' });
  } catch (err) {
    dispatch({ type: DISPLAY_ERROR, payload: err.message });
  }

  // refresh campaigns data

  dispatch({ type: TOGGLE_CAMPAIGN_LOADING, payload: false });
  dispatch({ type: TOGGLE_CAMPAIGN_VIEW, payload: false });
};

export const approveRequest = (index, contract) => async dispatch => {
  dispatch({ type: TOGGLE_APPROVE_LOADING, payload: true });

  try {
    const accounts = await web3.eth.getAccounts();

    await contract.methods.approveRequest(index).send({
      from: accounts[0]
    });

    dispatch({ type: DISPLAY_ERROR, payload: '' });
  } catch (err) {
    dispatch({ type: DISPLAY_ERROR, payload: err.message });
  }

  // refresh campaigns data

  dispatch({ type: TOGGLE_APPROVE_LOADING, payload: false });
};

export const completeRequest = (index, contract) => async dispatch => {
  dispatch({ type: TOGGLE_COMPLETE_LOADING, payload: true });

  try {
    const accounts = await web3.eth.getAccounts();

    await contract.methods.completeRequest(index).send({
      from: accounts[0]
    });

    dispatch({ type: DISPLAY_ERROR, payload: '' });
  } catch (err) {
    dispatch({ type: DISPLAY_ERROR, payload: err.message });
  }

  // refresh campaigns data

  dispatch({ type: TOGGLE_COMPLETE_LOADING, payload: false });
};
